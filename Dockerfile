FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /build/libs/ansibledockerspringboot-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8090
ENTRYPOINT ["java","-jar","/app.jar"]

